function loadAdmin() {
	var userAlreadyExists = Meteor.users.findOne({"emails.address": 'admin@test.com'});
	var user;
	if (!userAlreadyExists) {
		user = {
			name: "Admin",
			username: 'admin',
			email: 'admin@test.com',
			password: 'qweqwe'
		};

		var userId = Accounts.createUser(user);
		Roles.addUsersToRoles(userId, ["admin"]);
		console.log('admin created');
	}
}

Meteor.startup(function () {
	loadAdmin();
});