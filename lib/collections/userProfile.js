/**
 * Profile for User Schema.
 * Its almost same as Meteor.users.profile
 * User roles : admin, user
 */
Schemas.UserProfile = new SimpleSchema({
	name: {
		type: String,
		optional: true
	},
	phone: {
		type: String,
		optional: true
	},
	address: {
		type: [Object],
		optional: true,
		blackbox: true
	},
	status: {
		type: String,
		allowedValues: ["Active", "Inactive", "Archive"],
		defaultValue: "Active"
	}
});

/**
 * Users Schema
 * Same as Meteor.users
 * @type {SimpleSchema}
 */
Schemas.User = new SimpleSchema({
	username: {
		type: String,
		optional: true
	},
	emails: {
		type: Array,
		optional: true
	},
	"emails.$": {
		type: Object
	},
	"emails.$.address": {
		type: String,
		regEx: SimpleSchema.RegEx.Email
	},
	"emails.$.verified": {
		type: Boolean
	},
	profile: {
		type: Schemas.UserProfile,
		optional: true
	},
	createdAt: {
		type: Date,
		autoValue: function () {
			if (this.isInsert) {
				return new Date();
			}
			if (this.isUpsert) {
				return {$setOnInsert: new Date()};
			}
			this.unset();
		},
		denyUpdate: true,
		optional: true
	},
	services: {
		type: Object,
		optional: true,
		blackbox: true
	},
	roles: {
		type: [String],
		optional: true
	}

});

Meteor.users.attachSchema(Schemas.User);

/*
createdAt: {
	type: Date,
		autoValue: function () {
		if (this.isInsert) {
			return new Date();
		}
		if (this.isUpsert) {
			return {$setOnInsert: new Date()};
		}
		this.unset();
	},
	denyUpdate: true,
		optional: true
},
updatedAt: {
	type: Date,
		autoValue: function () {
		if (this.isUpdate) {
			return new Date();
		}
	},
	denyInsert: true,
		optional: true
}*/
