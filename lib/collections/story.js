Stories = new Mongo.Collection('stories');

Schemas.story = new SimpleSchema({
	title: {
		type: String
	},
	description: {
		type: String
	},
	createdAt: {
		type: Date,
		autoValue: function () {
			if (this.isInsert) {
				return new Date();
			}
			if (this.isUpsert) {
				return {$setOnInsert: new Date()};
			}
			this.unset();
		},
		denyUpdate: true
	},
	anonymous: {
		type : String,
		allowedValues: ['true', 'false'],
		optional: true
	},
	createdBy: {
		type: Object,
		blackbox: true,
		autoValue: function () {
			if (this.isInsert) {
				return Anonna.userDetail(Meteor.user());
			}
		},
		denyUpdate: true,
		optional: true
	}
});

