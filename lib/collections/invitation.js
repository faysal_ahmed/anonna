Invites = new Mongo.Collection('invites');

// Schemas.invite = new SimpleSchema({
// 	email: {
// 		type: String,
// 		regEx: SimpleSchema.RegEx.Email
// 	},
// 	inviteCode: {
// 		type: String
// 	},
// 	createdAt: {
// 		type: Date,
// 		autoValue: function () {
// 			if (this.isInsert) {
// 				return new Date();
// 			}
// 		}
// });

Schemas.invite = new SimpleSchema({
	email: {
		type: String,
		regEx: SimpleSchema.RegEx.Email
	},
	inviteCode: {
		type: String
	},
	createdAt: {
		type: Date,
		autoValue: function() {
			if (this.isInsert) {
				return new Date();
			}
		}
	}
});